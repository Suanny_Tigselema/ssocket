/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import views.ServerInterface;
/**
 *
 * @author Suanny
 */
public class MyThread extends Thread implements Runnable{
    views.ServerInterface interfaz;
    public MyThread(String name){
        super(name);
       // start();     
    }

    @Override
    public void run() {
        ServerSocket serverSocket;
        Socket server;
        try {
            serverSocket = new ServerSocket(5555);
            //System.out.println("Ya creó el ServerSocket");
            while (true)
            {
                try{
                    server = serverSocket.accept();
                    //System.out.println("Ya aceptó algo el servidor");
                    DataInputStream inputStream = new DataInputStream(server.getInputStream());
                    //txtMessages.setText(txtMessages.getText() + "\n" + inputStream.readUTF());
                //    System.out.println(inputStream.readUTF());
                    interfaz.txtMessages.setText(interfaz.txtMessages.getText() + "\n" + inputStream.readUTF());
                    server.close();
                }
                catch (IOException ex){
                    System.out.println (ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }  
}
