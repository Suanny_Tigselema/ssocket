/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

/**
 *
 * @author gugle
 */
import clases.MyThread;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.*;

public final class ServerInterface extends JFrame /*implements Runnable*/{

    private final JLabel lblTitle, lblMessages, lblTxt;
    /*, lblMessage*/
    //private final JTextField txtClient, txtIp; //txtMessage
    public static JTextArea txtMessages;
    private final JButton btnLimpiar, btnEnviar; //btnSubmit,
    private final JPanel panel;
    private final JTextField txtSms;
    ServerSocket serverSocket;
    Socket server;
    private MyThread myThread;
    
    public ServerInterface(String title) {
        lblTitle = new JLabel("Chat (Servidor)");
        lblMessages = new JLabel("Chat: ");
        txtMessages = new JTextArea(10, 25);
        lblTxt = new JLabel("Sms: ");
        btnLimpiar = new JButton("Limpiar");
        txtSms  = new JTextField (25);
        btnEnviar = new JButton("Enviar");
        btnEnviar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try (DataOutputStream outputStream = new DataOutputStream(server.getOutputStream())) {
                    String sms = txtSms.getText();
                    outputStream.writeUTF(sms);
                    outputStream.close();
                } catch (IOException ex) {
                    txtMessages.setText(txtMessages.getText() + "\n" + txtSms.getText() + " Message not sent.");
                    System.out.println(ex.getMessage());
                }

            }
        });
        panel = new JPanel();

        panel.add(lblTitle);
        panel.add(lblMessages);
        panel.add(txtMessages);
        panel.add(btnLimpiar);
        panel.add(lblTxt);
        panel.add(txtSms);
        panel.add(btnEnviar);

        this.setBounds(800, 350, 320, 410);
        this.add(panel);
        this.setVisible(true);
        this.setTitle(title);
        //listen();
        myThread = new MyThread ("Hilo");
        myThread.start();
    }

   /* public void listen() {
        ServerSocket serverSocket;
        Socket server;
        try {
            serverSocket = new ServerSocket(8081);
            //System.out.println("Ya creó el ServerSocket");
            while (true) {
                try {
                    server = serverSocket.accept();
                    //System.out.println("Ya aceptó algo el servidor");
                    DataInputStream inputStream = new DataInputStream(server.getInputStream());
                    txtMessages.setText(txtMessages.getText() + "\n" + inputStream.readUTF());
                    server.close();
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }*/

}
